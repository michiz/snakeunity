﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Snake : MonoBehaviour {
    public delegate void PlayerDelegate();
    public static event PlayerDelegate OnPlayerDied;
    public static event PlayerDelegate OnPlayerScored;


    public Vector2 startPos;

    // Current Movement Direction
    // (by default it moves to the right)
    Vector2 direction = Vector2.right;

    // Keep Track of Tail
    List<Transform> tail = new List<Transform>();

    // touch and mouse vector
    Vector2 mouseDownPos;
    Vector2 mouseUpPos;
    Vector2 mouseSum;
    Vector2 cardinalAxe = new Vector2(1, 0);
    float mousePhaseAngle;

    // Did the snake eat something?
    bool ate = false;
    // Tail Prefab
    public GameObject tailPrefab;
    public AudioSource moveAudio;
    public AudioSource scoreAudio;
    public AudioSource dieAudio;
    public GameObject foodPrefab;
    // Borders
    public Transform borderTop;
    public Transform borderBottom;
    public Transform borderLeft;
    public Transform borderRight;


    void OnEnable() {
        GameManager.OnGameStarted += OnGameStarted;
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
    }

    void OnDisable() {
        GameManager.OnGameStarted -= OnGameStarted;
        GameManager.OnGameOverConfirmed -= OnGameOverConfirmed;
    }

    void OnGameStarted() {
        direction = Vector2.right;
        tail = new List<Transform>();

        InvokeRepeating("Move", 0.1f, 0.1f);
        Spawn();
    }

    void OnGameOverConfirmed() {
        transform.localPosition = startPos;

        GameObject[] foods;
        GameObject[] tails;

        foods = GameObject.FindGameObjectsWithTag("Food");
        tails = GameObject.FindGameObjectsWithTag("Tail");
        foreach (GameObject food in foods) {
            Destroy(food);
        }

        foreach (GameObject tail in tails) {
            Destroy(tail);
        }
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            mouseDownPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0)) {
            mouseUpPos = Input.mousePosition;
            mouseSum = mouseDownPos - mouseUpPos;
            mousePhaseAngle = Vector2.SignedAngle(mouseSum, cardinalAxe);

            // switch direction touch / mouse
            if ((mousePhaseAngle > 135.0f && mousePhaseAngle < 180.0f) || (mousePhaseAngle > -180.0f && mousePhaseAngle < -135.0f)) {
                if (direction != -Vector2.right) {

                    moveAudio.Play();
                    direction = Vector2.right;
                }
            } else if (mousePhaseAngle > -135.0f && mousePhaseAngle < -45.0f) {
                if (direction != Vector2.up) {
                    moveAudio.Play();
                    direction = -Vector2.up;    // '-up' means 'down'
                }
            } else if ((mousePhaseAngle > -45.0f && mousePhaseAngle < 0.0f) || (mousePhaseAngle > 0.0f && mousePhaseAngle < 45.0f)) {
                if (direction != Vector2.right) {
                    moveAudio.Play();
                    direction = -Vector2.right; // '-right' means 'left'
                }
            } else if (mousePhaseAngle > 45.0f && mousePhaseAngle < 135.0f) {
                if (direction != -Vector2.up) {
                    moveAudio.Play();
                    direction = Vector2.up;
                }
            }
        }

        // switch direction KEYBOARD
        if (Input.GetKeyDown(KeyCode.RightArrow) && direction != -Vector2.right) {
            moveAudio.Play();
            direction = Vector2.right;
        } else if (Input.GetKeyDown(KeyCode.DownArrow) && direction != Vector2.up) {
            moveAudio.Play();
            direction = -Vector2.up;    // '-up' means 'down'
        } else if (Input.GetKeyDown(KeyCode.LeftArrow) && direction != Vector2.right) {
            moveAudio.Play();
            direction = -Vector2.right; // '-right' means 'left'
        } else if (Input.GetKeyDown(KeyCode.UpArrow) && direction != -Vector2.up) {
            moveAudio.Play();
            direction = Vector2.up;
        }
    }

    void Move() {
        // Save current position (gap will be here)
        Vector2 headPosBeforeMove = transform.position;

        // Move head into new direction
        transform.Translate(direction);

        // Ate something? Then insert new Element into gap
        if (ate) {
            // Load Prefab into the world
            GameObject newTailObject = (GameObject)Instantiate(tailPrefab,
                                                               headPosBeforeMove,
                                                               Quaternion.identity);



            // Keep track of it in our tail list
            tail.Insert(0, newTailObject.transform);

            Spawn();

            // Reset the flag
            ate = false;
        }


        // Do we have a Tail?
        if (tail.Count > 0) {
            // Move last Tail Element to where the Head was
            tail.Last().position = headPosBeforeMove;

            // Add to front of list, remove from the back
            tail.Insert(0, tail.Last());
            tail.RemoveAt(tail.Count - 1);
        }

    }

    public void Spawn() {
        // x position between left & right border
        float x = (float)Random.Range(borderLeft.position.x,
                                      borderRight.position.x);
        // round to nearest .5 (border x are aligned to .5) --> maybe
        // x = Math.Round(x * 2, MidpointRounding.AwayFromZero) / 2;

        // y position between top & bottom border
        int y = (int)Random.Range(borderBottom.position.y,
                                  borderTop.position.y);

        // Instantiate the food at (x, y)
        Instantiate(foodPrefab,
                    new Vector2(x, y),
                    Quaternion.identity); // default rotation
    }

    void OnTriggerEnter2D(Collider2D coll) {
        // Food?
        // Note: we use coll.name.StartsWith because the food is called 'FoodPrefab(Clone)' after instantiating it.The more elegant way to find out if coll is food or not would be by using a Tag, but for the sake of simplicity we will use string comparison in this Tutorial.
        if (coll.tag == "Food") {
            // Get longer in next Move call
            ate = true;
            scoreAudio.Play();
            // Remove the Food
            Destroy(coll.gameObject);

            OnPlayerScored(); // event sent to GameManager
        }

        // Collided with Tail or Border
        else {
            dieAudio.Play();
            CancelInvoke("Move");
            OnPlayerDied(); // event sent to GameManager
        }
    }
}
