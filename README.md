# snakeUnity

A little snake game made with the Unity3D game engine.

If you want to learn how its made, check out those tutorials:

https://noobtuts.com/unity/2d-snake-game --> Basic snake logic

https://www.youtube.com/watch?v=A-GkNM8M5p8 --> GameController / Menu logic / Publishing

## Installation:
1. Download Unity for your OS:
https://unity3d.com/de/get-unity/download
2. Clone this project into some folder
3. In Unity -> 'Open Project..' and target the src folder.

## Try it in the browser!

The WebGL build is mounted on this gitlab page:

https://michiz.gitlab.io/snakeunity

## Game controls:

Keyboard:

Arrow-Keys

Touch/Mouse:

Swipe

